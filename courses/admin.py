from django.contrib import admin
from courses.models import Course, Subcourse, Appointment, Transaksi,Review

# Register your models here.
admin.site.register(Course)
admin.site.register(Subcourse)
admin.site.register(Appointment)
admin.site.register(Transaksi)
admin.site.register(Review)