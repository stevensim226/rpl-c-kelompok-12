from django.forms import ModelForm, HiddenInput
from courses.models import Review

class Review_form(ModelForm):

    class Meta:
        model = Review
        fields = ('bintang', 'judul', 'deskripsi')
        widgets = {
            'nama' : HiddenInput(),
            'subcourse': HiddenInput(),
        }