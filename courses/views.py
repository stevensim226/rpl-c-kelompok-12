from django.shortcuts import render, redirect
from django.db.models import Avg
from courses.forms import Review_form
from users.models import StudentProfile
from courses.models import Course, Subcourse, Transaksi, Appointment, Review
from uuid import uuid4
from datetime import datetime
from django.contrib import messages

# Create your views here.
def courses_list(request):
    context = {
        "courses" : [
            (course, len(course.subcourse_set.all())) for course
            in Course.objects.all()
        ]
    }

    return render(request, "courses/courses_list.html", context=context)

def subcourses_list(request, course_id):
    context = {
        "subcourses" : Course.objects.get(id=course_id).subcourse_set.all()
    }

    return render(request, "courses/subcourses_list.html", context=context)

def subcourses_detail(request, subcourse_id):
    if request.method == "POST":
        can_review = request.user.studentprofile.transaksi_set.filter(subcourse_dibeli=subcourse_id).count()
        reviewed = Review.objects.all().filter(subcourse_id=subcourse_id).count()
        cannot_review = False
        if can_review < reviewed:
            cannot_review = True
        form_review = Review_form(request.POST)
        if cannot_review == True:
            messages.add_message(request, messages.ERROR, "Penambahan Review gagal, harap dicoba lagi")
            return redirect('courses:subcourse_detail', subcourse_id=subcourse_id)
        else :
            if form_review.is_valid():
                instance = form_review.save(commit=False)
                instance.nama = request.user.studentprofile.get_nama()
                instance.subcourse = Subcourse.objects.get(id=subcourse_id)
                instance.save()
            return redirect('courses:subcourse_detail', subcourse_id=subcourse_id)
    else :
        can_review = request.user.studentprofile.transaksi_set.filter(subcourse_dibeli=subcourse_id).count()
        reviewed = Review.objects.all().filter(subcourse_id=subcourse_id).count()
        cannot_review = False
        if can_review < reviewed:
            cannot_review = True
        reviews = Review.objects.filter(subcourse__id = subcourse_id)
        try :
            bintang_avg = round(list(reviews.aggregate(Avg('bintang')).values())[0], 2)
        except :
            bintang_avg = "X"
        form_review = Review_form()
        context = {
            "subcourse_id" : subcourse_id,
            "subcourse": Subcourse.objects.get(id=subcourse_id),
            "reviews": reviews,
            "form_review": form_review,
            "bintang_avg" : bintang_avg,
            "cannot_review": cannot_review,
        }
        return render(request, "courses/subcourse_detail.html", context=context)

def checkout(request, subcourse_id):
    if request.method == "POST":
        subcourse_dibeli = Subcourse.objects.get(id=subcourse_id)
        
        request.user.studentprofile.beli(
            subcourse_dibeli
        )
        messages.add_message(request, messages.SUCCESS, "Terima kasih atas pembelian sesi kursus!")

        return redirect("dashboard:upcoming")
    
    context = {
        "subcourse" : Subcourse.objects.get(id=subcourse_id),
        "nominal_after" : request.user.studentprofile.saldo - Subcourse.objects.get(id=subcourse_id).harga,
        
    }

    context["is_negatif"] = context["nominal_after"] < 0

    return render(request, "courses/checkout.html", context=context)
