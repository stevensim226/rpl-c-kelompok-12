from django.urls import path
from . import views

app_name = "courses"

urlpatterns = [
    path("", views.courses_list, name="courses_list"),
    path("<int:course_id>/subcourses", views.subcourses_list, name="subcourses_list"),
    path("subcourses/<int:subcourse_id>", views.subcourses_detail, name="subcourse_detail"),
    path("subcourses/<int:subcourse_id>/checkout", views.checkout, name="checkout"),
]