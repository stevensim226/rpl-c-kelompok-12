from django.db import models

# Create your models here.
class Course(models.Model):
    nama = models.CharField(max_length=255)
    deskripsi = models.CharField(max_length=2047)

class Subcourse(models.Model):
    harga = models.IntegerField()
    nama = models.CharField(max_length=255)
    deskripsi = models.CharField(max_length=2047)
    jam_dibutuhkan = models.IntegerField()
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

class Transaksi(models.Model):
    waktu_pembelian = models.DateTimeField(auto_now_add=True)
    metode_pembayaran = models.CharField(max_length=63)
    jumlah_pembayaran = models.IntegerField()
    student_pembeli = models.ForeignKey("users.StudentProfile", on_delete=models.CASCADE)
    subcourse_dibeli = models.ForeignKey(Subcourse, on_delete=models.CASCADE)
    status = models.CharField(max_length=63)

class Appointment(models.Model):
    status = models.CharField(max_length=63)
    waktu_kursus = models.DateTimeField()
    kode_video_call = models.CharField(max_length=127)
    bagian_dari = models.ForeignKey(Transaksi, on_delete=models.CASCADE)

bintang_choices = [
    (1,1),
    (2,2),
    (3,3),
    (4,4),
    (5,5),
]

class Review(models.Model):
    nama = models.CharField(max_length=64)
    bintang = models.IntegerField(choices=bintang_choices)
    judul = models.CharField(max_length=64)
    deskripsi = models.TextField()
    subcourse = models.ForeignKey(Subcourse, on_delete=models.CASCADE, related_name='subcourse')