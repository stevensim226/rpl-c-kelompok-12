from django.test import TestCase, LiveServerTestCase, Client
from django.urls import reverse, resolve

from django.contrib.auth.models import User

from users.models import StudentProfile
from courses.views import courses_list, subcourses_list, subcourses_detail, checkout

from courses.models import Course, Subcourse, Transaksi, Appointment
from datetime import datetime, timedelta
from uuid import uuid4

class TestModels(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("user123", password="passworduser123")
        self.new_user.save()

        self.new_profile = StudentProfile.objects.create(
            user = self.new_user,
            deskripsi = "Hello World",
            nama = "John Doe",
            foto_profil = "https://via.placeholder.com/300/",
            bidang_minat = "Literatur"
        )

    def test_course_creation(self):
        Course.objects.create(
            nama="Fisika",
            deskripsi="Fisika dasar, listrik, kuantum, dll."
        )

        self.assertEquals(
            1,
            len(Course.objects.all())
        )

    def test_subcourse_creation(self):
        sample_course = Course.objects.create(
            nama="Fisika",
            deskripsi="Fisika dasar, listrik, kuantum, dll."
        )

        Subcourse.objects.create(
            nama="Fisika Listrik",
            deskripsi="Fisika listrik, hukum kirchoff, hukum ohm, dll",
            jam_dibutuhkan=6,
            course=sample_course,
            harga=500000
        )

        self.assertEquals(
            1,
            len(Subcourse.objects.all())
        )

    def test_transaction_creation(self):
        sample_course = Course.objects.create(
            nama="Fisika",
            deskripsi="Fisika dasar, listrik, kuantum, dll."
        )

        sample_subcourse = Subcourse.objects.create(
            nama="Fisika Listrik",
            deskripsi="Fisika listrik, hukum kirchoff, hukum ohm, dll",
            jam_dibutuhkan=6,
            course=sample_course,
            harga=500000
        )

        Transaksi.objects.create(
            metode_pembayaran="Saldo",
            jumlah_pembayaran=500000,
            student_pembeli=self.new_profile,
            subcourse_dibeli=sample_subcourse,
            status="Dibayar"
        )

        self.assertEquals(
            1,
            len(Transaksi.objects.all())
        )

    def test_appointment_creation(self):
        def test_transaction_creation(self):
            sample_course = Course.objects.create(
                nama="Fisika",
                deskripsi="Fisika dasar, listrik, kuantum, dll."
            )

            sample_subcourse = Subcourse.objects.create(
                nama="Fisika Listrik",
                deskripsi="Fisika listrik, hukum kirchoff, hukum ohm, dll",
                jam_dibutuhkan=6,
                course=sample_course,
                harga=500000
            )

            sample_transaksi = Transaksi.objects.create(
                metode_pembayaran="Saldo",
                jumlah_pembayaran=500000,
                student_pembeli=self.new_profile,
                subcourse_dibeli=sample_subcourse,
                status="Dibayar"
            )

            Appointment.objects.create(
                status="Belum Dimulai",
                waktu_kursus=datetime.now() + timedelta(days=1),
                kode_video_call=str(uuid4()),
                bagian_dari = sample_transaksi
            )

            self.assertEquals(
                1,
                len(Appointment.objects.all())
            )


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_url = reverse("users:login")

        self.new_user = User.objects.create_user("user123", password="passworduser123")
        self.new_user.save()

        self.new_profile = StudentProfile.objects.create(
            user = self.new_user,
            deskripsi = "Hello World",
            nama = "John Doe",
            foto_profil = "https://via.placeholder.com/300/",
            bidang_minat = "Literatur"
        )

        # Login sebagai user123
        self.client.post(
            self.login_url, data = {
                "username" : "user123",
                "password" : "passworduser123"
            }
        )

        self.sample_course = Course.objects.create(
                nama="Fisika",
                deskripsi="Fisika dasar, listrik, kuantum, dll."
            )

        self.sample_subcourse = Subcourse.objects.create(
            nama="Fisika Listrik",
            deskripsi="Fisika listrik, hukum kirchoff, hukum ohm, dll",
            jam_dibutuhkan=6,
            course=self.sample_course,
            harga=500000
        )

        self.sample_transaksi = Transaksi.objects.create(
            metode_pembayaran="Saldo",
            jumlah_pembayaran=500000,
            student_pembeli=self.new_profile,
            subcourse_dibeli=self.sample_subcourse,
            status="Dibayar"
        )

        self.sample_appointment = Appointment.objects.create(
            status="Pending",
            waktu_kursus=datetime.now() + timedelta(days=1),
            kode_video_call=str(uuid4()),
            bagian_dari = self.sample_transaksi
        )

        self.courses_list_url = reverse("courses:courses_list")
        self.subcourses_list_url = reverse("courses:subcourses_list", args=[self.sample_course.id])
        self.subcourse_detail_url = reverse("courses:subcourse_detail", args=[self.sample_subcourse.id])
        self.checkout_url = reverse("courses:checkout", args=[self.sample_subcourse.id])

    def test_courses_list(self):
        response = self.client.get(
            self.courses_list_url
        )
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "courses/courses_list.html")
        self.assertTrue(
            "Fisika dasar, listrik, kuantum, dll." in str(response.content)
        )

    def test_subcourses_list(self):
        response = self.client.get(
            self.subcourses_list_url
        )
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "courses/subcourses_list.html")
        self.assertTrue(
            "Fisika listrik, hukum kirchoff" in str(response.content)
        )

    def test_subcourse_detail(self):
        response = self.client.get(
            self.subcourse_detail_url
        )
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "courses/subcourse_detail.html")

        self.assertTrue(
            "Fisika listrik, hukum kirchoff" in str(response.content)
        )

    def test_checkout_page(self):
        response = self.client.get(
            self.checkout_url
        )
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "courses/checkout.html")

    def test_checkout_purchase(self):
        response = self.client.post(
            self.checkout_url
        )

        self.assertEquals(
            self.sample_subcourse.jam_dibutuhkan + 1,
            len(Appointment.objects.all())
        )



class TestUrls(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("user123", password="passworduser123")
        self.new_user.save()

        self.new_profile = StudentProfile.objects.create(
            user = self.new_user,
            deskripsi = "Hello World",
            nama = "John Doe",
            foto_profil = "https://via.placeholder.com/300/",
            bidang_minat = "Literatur"
        )

        self.sample_course = Course.objects.create(
                nama="Fisika",
                deskripsi="Fisika dasar, listrik, kuantum, dll."
            )

        self.sample_subcourse = Subcourse.objects.create(
            nama="Fisika Listrik",
            deskripsi="Fisika listrik, hukum kirchoff, hukum ohm, dll",
            jam_dibutuhkan=6,
            course=self.sample_course,
            harga=500000
        )

        self.sample_transaksi = Transaksi.objects.create(
            metode_pembayaran="Saldo",
            jumlah_pembayaran=500000,
            student_pembeli=self.new_profile,
            subcourse_dibeli=self.sample_subcourse,
            status="Dibayar"
        )

        self.sample_appointment = Appointment.objects.create(
            status="Pending",
            waktu_kursus=datetime.now(),
            kode_video_call=str(uuid4()),
            bagian_dari = self.sample_transaksi
        )

        self.courses_list_url = reverse("courses:courses_list")
        self.subcourses_list_url = reverse("courses:subcourses_list", args=[self.sample_course.id])
        self.subcourse_detail_url = reverse("courses:subcourse_detail", args=[self.sample_subcourse.id])
        self.checkout_url = reverse("courses:checkout", args=[self.sample_subcourse.id])

    def test_courses_list_url(self):
        courses_list_func = resolve(self.courses_list_url).func
        self.assertEquals(courses_list_func, courses_list)

    def test_subcourses_list_url(self):
        subcourses_list_func = resolve(self.subcourses_list_url).func
        self.assertEquals(subcourses_list_func, subcourses_list)

    def test_subcourse_detail_url(self):
        subcourses_detail_func = resolve(self.subcourse_detail_url).func
        self.assertEquals(subcourses_detail_func, subcourses_detail)

    def test_checkout_url(self):
        checkout_func = resolve(self.checkout_url).func
        self.assertEquals(checkout, checkout_func)
