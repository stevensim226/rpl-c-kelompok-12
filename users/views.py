from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate

from users.models import StudentProfile


# Create your views here.
def landing(request):
    if request.user.is_authenticated:
        return redirect("dashboard:upcoming")

    return render(request, "users/landing.html")

def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("users:landing")

def login_view(request):
    if request.user.is_authenticated:
        return redirect("dashboard:upcoming")
    

    if request.method == "POST":
        user = authenticate(
            username = request.POST.get("username", None),
            password = request.POST.get("password", None)
        )

        if user is not None:
            login(request, user)
            return redirect("dashboard:upcoming")

        else:
            messages.add_message(request, messages.WARNING, f"Username atau password salah.")

    return render(request, "users/login.html")

def register(request):
    if request.user.is_authenticated:
        return redirect("dashboard:upcoming")

    if request.method == "POST":
        if (request.POST["password1"] != request.POST["password2"]):
            messages.add_message(request, messages.WARNING, "Password di kedua input tidak sama!")
        elif User.objects.filter(username=request.POST["username"]):
            messages.add_message(request, messages.WARNING, "Username sudah terdaftar, silahkan login atau ambil username lain")
        else: # Case sukses register
            
            new_user = User.objects.create_user(
                request.POST["username"],
                password=request.POST["password1"],
                email=request.POST["email"]    
            )

            new_user.save()

            # Bikin profile baru
            StudentProfile.objects.create(
                user = new_user,
                nama = request.POST["username"],
            )


            messages.add_message(request, messages.SUCCESS, "Pendaftaran sukses, silahkan login.")
            return redirect("users:login")

            
    return render(request, "users/register.html")

def profile(request):
    if not request.user.is_authenticated:
        return redirect("users:login")
    user = StudentProfile.objects.get(user=request.user.id)
    context = {"user" : user}
    return render(request, "users/profile.html", context)

def edit(request):
    if not request.user.is_authenticated:
        return redirect("users:login")
    user = StudentProfile.objects.get(user=request.user.id)
    if request.method == "POST":
        user.edit_profil(request.POST["deskripsi"], user.foto_profil, request.POST["minat"])
        return redirect("users:profile")
    context = {"user" : user}
    return render(request, "users/edit.html", context)

def saldo(request):
    if request.method == "POST":
        saldo_dibeli = int(request.POST.get('tambah_saldo'))
        request.user.studentprofile.isi_saldo(saldo_dibeli)
        context = {
            "saldo": request.user.studentprofile.saldo,
            "tambah_saldo": saldo_dibeli,
        }
        return render(request, "users/saldo_success.html", context=context)
    context = {
        "saldo" : request.user.studentprofile.saldo
    }
    return render(request, "users/saldo.html", context=context)


