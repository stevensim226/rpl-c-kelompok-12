from django.urls import path
from . import views

app_name = "users"

urlpatterns = [
    path("", views.landing, name="landing"),
    path("login", views.login_view, name="login"),
    path("register", views.register, name="register"),
    path("profile", views.profile, name="profile"),
    path("edit", views.edit, name="edit"),
    path("logout", views.logout_view, name="logout"),
    path("saldo/", views.saldo, name="saldo"),
]