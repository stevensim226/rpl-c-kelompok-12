from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from courses.models import Subcourse, Appointment, Transaksi
from uuid import uuid4

class StudentProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    deskripsi = models.TextField(default="Halo! Saya siap belajar.")
    nama = models.CharField(max_length=255, default="Anonim")
    foto_profil = models.URLField(default="https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png")
    bidang_minat = models.CharField(max_length=255, default="-")
    saldo = models.IntegerField(default=0)

    def edit_profil(self, deskripsi=None, foto_profil=None, bidang_minat=None):
        if deskripsi:
            self.deskripsi = deskripsi

        if foto_profil:
            self.foto_profil = foto_profil

        if bidang_minat:
            self.bidang_minat = bidang_minat

        self.save()

    def get_upcoming_apt(self):
        return Appointment.objects.filter(
            bagian_dari__student_pembeli=self,
            waktu_kursus__gte=datetime.now()
        )

    def beli(self, subcourse_dibeli):
        self.saldo -= subcourse_dibeli.harga
        self.save()

        # Buat instance Transakdi
        new_transaksi = Transaksi.objects.create(
            metode_pembayaran="Saldo",
            jumlah_pembayaran=subcourse_dibeli.harga,
            student_pembeli=self,
            subcourse_dibeli=subcourse_dibeli,
            status="Sukses"
        )

        # Buat instance Appointment
        for ii in range(subcourse_dibeli.jam_dibutuhkan):
            Appointment.objects.create(
                status="Pending",
                waktu_kursus=datetime.now() + timedelta(days=ii+1,minutes=30),
                kode_video_call=str(uuid4()),
                bagian_dari=new_transaksi
            )

    def isi_saldo(self, saldo_dibeli):
        self.saldo += saldo_dibeli
        self.save()

    def get_nama(self):
        return self.nama

class KontenKreator(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nama = models.CharField(max_length=255, default="Anonim")

    def __str__(self):
        return self.nama