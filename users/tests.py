from django.test import TestCase, LiveServerTestCase, Client
from django.urls import reverse, resolve

from django.contrib.auth.models import User

from users.models import StudentProfile
from users.views import landing, login_view, logout_view, register, profile, edit, saldo
from courses.models import Course, Subcourse, Transaksi, Appointment

class TestModels(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("user123", password="passworduser123")
        self.new_user.save()

        self.new_profile = StudentProfile.objects.create(
            user = self.new_user,
            deskripsi = "Hello World",
            nama = "John Doe",
            foto_profil = "https://via.placeholder.com/300/",
            bidang_minat = "Literatur",
            saldo = 100000,
        )

    def test_edit_profil_berhasil(self):
        self.new_profile.edit_profil(
            deskripsi = "New Desc",
            foto_profil = "https://via.placeholder.com/500/",
            bidang_minat = "Arts"
        )
        self.assertEquals(
            StudentProfile.objects.get(user=self.new_user).deskripsi
            , "New Desc")

    def test_isi_saldo(self):
        self.new_profile.isi_saldo(
            saldo_dibeli= 100000,
        )
        self.assertEquals(
            StudentProfile.objects.get(user=self.new_user).saldo
            , 200000)


class TestStudentProfileFunctions(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("user123", password="passworduser123")
        self.new_user.save()

        self.new_profile = StudentProfile.objects.create(
            user = self.new_user,
            deskripsi = "Hello World",
            nama = "John Doe",
            foto_profil = "https://via.placeholder.com/300/",
            bidang_minat = "Literatur"
        )

        self.sample_course = Course.objects.create(
                nama="Fisika",
                deskripsi="Fisika dasar, listrik, kuantum, dll."
            )

        self.sample_subcourse = Subcourse.objects.create(
            nama="Fisika Listrik",
            deskripsi="Fisika listrik, hukum kirchoff, hukum ohm, dll",
            jam_dibutuhkan=6,
            course=self.sample_course,
            harga=500000
        )

    def test_nama(self):
        self.assertEquals(self.new_profile.get_nama(), "John Doe")

    # Apakah method beli berhasil dengan membuat appointment baru?
    def test_beli(self):
        self.new_profile.beli(self.sample_subcourse)

        self.assertEquals(
            6,
            len(Appointment.objects.all())
        )
        self.assertEquals(1, len(Transaksi.objects.all()))

    # Apakah method dapat appointment berhasil setelah beli?
    def test_get_upcoming_apt(self):
        self.new_profile.beli(self.sample_subcourse)
        self.assertEquals(
            self.sample_subcourse.jam_dibutuhkan,
            len(self.new_profile.get_upcoming_apt())
        )


class TestViews(TestCase):
    def setUp(self):
        # Dummy data
        self.client = Client()
        self.new_user = User.objects.create_user("user123", password="passworduser123")
        self.new_user.save()
        self.new_profile = StudentProfile.objects.create(
            user = self.new_user,
            deskripsi = "Hello World",
            nama = "John Doe",
            foto_profil = "https://via.placeholder.com/300/",
            bidang_minat = "Literatur",
            saldo = 100000,
        )

        # URLs
        self.landing_url = reverse("users:landing")
        self.login_url = reverse("users:login")
        self.register_url = reverse("users:register")
        self.profile_url = reverse("users:profile")
        self.edit_url = reverse("users:edit")
        self.logout_url = reverse("users:logout")
        self.saldo_url = reverse("users:saldo")

    def test_landing_page_loads(self):
        response = self.client.get(self.landing_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "users/landing.html")

    def test_login_page_loads(self):
        response = self.client.get(self.login_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "users/login.html")
        

    def test_register_page_loads(self):
        response = self.client.get(self.register_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "users/register.html")

    def test_profile_page_loads(self):
        response = self.client.post(
            self.login_url, data = {
                "username" : "user123",
                "password" : "passworduser123"
            }
        )
        response = self.client.get(self.profile_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "users/profile.html")
        self.assertContains(response, "Halo! Saya John Doe")
        self.assertContains(response, "Hello World")
        self.assertContains(response, "Literatur")

    def test_profile_not_authenticated(self):
        response = self.client.get(self.profile_url, follow=True)
        self.assertRedirects(response, "/login")
        self.assertTemplateUsed(response, "users/login.html")
        self.assertTemplateNotUsed(response, "users/profile.html")

    def test_edit_page_loads(self):
        response = self.client.post(
            self.login_url, data = {
                "username" : "user123",
                "password" : "passworduser123"
            }
        )
        response = self.client.get(self.edit_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "users/edit.html")
        self.assertContains(response, "Edit profile untuk John Doe")

    def test_edit_succesful(self):
        response = self.client.post(
            self.login_url, data = {
                "username" : "user123",
                "password" : "passworduser123"
            }
        )

        response = self.client.post(
            self.edit_url, data = {
                "deskripsi" : "deskripsi baru",
                "minat" : "minat baru, yang lama sudah dilupakan"
            }, follow=True
        )
        
        self.assertRedirects(response, "/profile")
        self.assertTemplateUsed(response, "users/profile.html")
        self.assertContains(response, "deskripsi baru")
        self.assertContains(response, "minat baru, yang lama sudah dilupakan")

    def test_edit_not_authenticated(self):
        response = self.client.get(self.edit_url, follow=True)
        self.assertRedirects(response, "/login")
        self.assertTemplateUsed(response, "users/login.html")
        self.assertTemplateNotUsed(response, "users/edit.html")

    def test_login_and_logout_success_and_redirects(self):
        response = self.client.post(
            self.login_url, data = {
                "username" : "user123",
                "password" : "passworduser123"
            }
        )

        self.assertEquals(response.status_code, 302)

        response = self.client.get(
            self.logout_url, follow=True
        )

        self.assertTemplateUsed(response, "users/landing.html")

    def test_pages_redirects_if_authenticated(self):
        self.client.post(
            self.login_url, data = {
                "username" : "user123",
                "password" : "passworduser123"
            }
        )

        response = self.client.get(self.landing_url)
        self.assertEquals(response.status_code, 302)
        self.assertTemplateUsed("dashboard/upcoming.html")

        response = self.client.get(self.login_url)
        self.assertEquals(response.status_code, 302)

        response = self.client.get(self.register_url)
        self.assertEquals(response.status_code, 302)

    def test_login_fails(self):
        response = self.client.post(
            self.login_url, data = {
                "username" : "user222",
                "password" : "passworduser123"
            }
        )

        self.assertTrue("Username atau password salah" in str(response.content))

    def test_register_success(self):
        response = self.client.post(
            self.register_url, data = {
                "username" : "newuser123",
                "password1" : "password1234",
                "password2" : "password1234",
                "email" : "newuser123@mailer.com"
            }
        )

        self.assertEquals(response.status_code, 302)
        self.assertEquals(len(User.objects.all()), 2) # Apakah instance user terbuat?

    def test_register_fails(self):
        response = self.client.post(
            self.register_url, data = {
                "username" : "newuser123",
                "password1" : "password1234",
                "password2" : "password",
                "email" : "newuser123@mailer.com"
            }
        )

        self.assertEquals(response.status_code, 200)
        self.assertTrue("Password di kedua input tidak sama!" in str(response.content))
        self.assertEquals(len(User.objects.all()), 1) # User tidak boleh dibuat karena fail

    def test_register_existing_user(self):
        response = self.client.post(
            self.register_url, data = {
                "username" : "user123",
                "password1" : "password1234",
                "password2" : "password1234",
                "email" : "newuser123@mailer.com"
            }
        )

        self.assertEquals(response.status_code, 200)
        print(response.content)
        self.assertTrue("Username sudah terdaftar, silahkan login atau ambil username lain" in str(response.content))

class TestUrls(TestCase):
    def setUp(self):
        self.landing_url = reverse("users:landing")
        self.login_url = reverse("users:login")
        self.logout_url = reverse("users:logout")
        self.register_url = reverse("users:register")
        self.profile_url = reverse("users:profile")
        self.edit_url = reverse("users:edit")
    
    def test_landing_url(self):
        landing_func = resolve(self.landing_url).func
        self.assertEquals(landing, landing_func)

    def test_register_url(self):
        register_func = resolve(self.register_url).func
        self.assertEquals(register, register_func)

    def test_login_url(self):
        login_func = resolve(self.login_url).func
        self.assertEquals(login_view, login_func)

    def test_logout_url(self):
        logout_func = resolve(self.logout_url).func
        self.assertEquals(logout_view, logout_func)

    def test_profile_url(self):
        profile_func = resolve(self.profile_url).func
        self.assertEquals(profile, profile_func)

    def test_edit_url(self):
        edit_func = resolve(self.edit_url).func
        self.assertEquals(edit, edit_func)

    def saldo_url(self):
        saldo_funct = resolve(self.saldo_url).func
        self.assertEquals(saldo, saldo_funct)