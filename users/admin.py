from django.contrib import admin
from users.models import StudentProfile, KontenKreator

admin.site.register(StudentProfile)
admin.site.register(KontenKreator)