from django.urls import path
from . import views

app_name = "konten"

urlpatterns = [
    path("", views.viewKonten, name="listKonten"),
    path("add", views.addKonten, name="addKonten"),
    path("delete/<int:id>", views.deleteKonten, name="deleteKonten"),
    path("publish/<int:id>", views.publishKonten, name="publishKonten"),
]