from django.db import models
from django.contrib.auth.models import User
import uuid

# Create your models here.
class KontenKreatif(models.Model):
    id = models.AutoField(primary_key=True)
    isPublished = models.BooleanField(default=False)
    tanggalPublish = models.DateTimeField()
    judul = models.CharField(max_length=50)
    isi = models.TextField()
    tanggalPenulisan = models.DateTimeField(auto_now_add=True, editable=False)
    penulis = models.ForeignKey(
        'users.KontenKreator',
        on_delete=models.CASCADE
    )
    
    def publish(self):
        self.isPublished = True

    def __str__(self):
        return self.judul
