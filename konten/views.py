from django.shortcuts import render, redirect
from django.utils import timezone
from django.forms import ValidationError
from .models import KontenKreatif
from users.models import KontenKreator
from django.contrib import messages

import datetime

month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']

# Create your views here.
def viewKonten(request):
    user = request.user
    currentDate = datetime.datetime.now(tz=timezone.get_current_timezone())
    context = {"konten" : KontenKreatif.objects.filter(isPublished=True), "kreator" : user.is_authenticated and KontenKreator.objects.filter(user=user).count() == 1 or user.is_superuser}
    for konten in context["konten"]:
        konten.tanggalPublish = str(konten.tanggalPublish.day) + ' ' + str(month[konten.tanggalPublish.month-1]) + ' ' + str(konten.tanggalPublish.year)
        if not request.user.is_authenticated:
            konten.isi = konten.isi[:300]
    for konten in KontenKreatif.objects.all():
        thisKonten = KontenKreatif.objects.filter(id=konten.id)
        if context["kreator"]:
            if KontenKreator.objects.get(user=request.user).id == konten.penulis.id or request.user.is_superuser and konten.isPublished == False:
                context["konten"] |= thisKonten
        if konten.tanggalPublish <= currentDate and konten.isPublished == False:
            konten.publish()
            konten.save()
            context["konten"] |= thisKonten
    return render(request, "viewKonten.html", context)


def addKonten(request):
    user = request.user
    KontenKreators = KontenKreator.objects.all()
    context = {"kreator" : user.is_authenticated and KontenKreator.objects.filter(user=user).count() == 1 or user.is_superuser}

    if request.method == 'POST':
        penuliss = KontenKreator.objects.filter(user = user)[:1]
        penulis = penuliss[0]

        judul = request.POST.get('judul')
        isi = request.POST.get('isi')
        tanggalPublish = request.POST.get('tanggalPublish')

        try: 
            kontenKreatif = KontenKreatif.objects.create(tanggalPublish = tanggalPublish, judul = judul, \
            isi = isi, penulis = penulis)
            kontenKreatif.save()
        except ValidationError:
            messages.error(request, "Terdapat kesalahan dalam pengisian formulir, silahkan isi ulang formulir!")

            return render(request, "addKonten.html", context)

        messages.success(request, "Konten berhasil ditambahkan")

        return redirect("konten:listKonten")

    return render(request, "addKonten.html", context)

def deleteKonten(request, id):
    user = request.user
    objectKonten = KontenKreatif.objects.get(id = id)
    penuliss = KontenKreator.objects.filter(user = user)[:1]
    penulis = penuliss[0]

    if penulis.user == user:
        objectKonten.delete()
        messages.success(request, "Konten berhasil dihapus")

        return redirect("konten:listKonten")
    
    messages.error(request, "Konten tidak berhasil dihapus")

    return redirect("konten:listKonten")

def publishKonten(request, id):
    user = request.user
    objectKonten = KontenKreatif.objects.get(id = id)
    penuliss = KontenKreator.objects.filter(user = user)[:1]
    penulis = penuliss[0]

    if penulis.user == user:
        objectKonten.publish()
        objectKonten.save()
        messages.success(request, "Konten berhasil publish")

        return redirect("konten:listKonten")

    messages.error(request, "Konten tidak berhasil dipublish")

    return redirect("konten:listKonten")
