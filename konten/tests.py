from django.test import TestCase, LiveServerTestCase, Client
from django.urls import reverse, resolve
import datetime

from django.contrib.auth.models import User

from konten.models import KontenKreatif
from users.models import KontenKreator, StudentProfile
from .views import viewKonten, addKonten, deleteKonten, publishKonten
from users.views import login_view

# Create your tests here.
class KontenTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='rplkelompok12', password ='ayeaye321')
        self.user.save()

        self.new_profile = KontenKreator.objects.create(
            user = self.user,
            nama = "Fasilkom"
        )

        self.new_konten = KontenKreatif.objects.create(
            penulis = self.new_profile,
            tanggalPublish = datetime.datetime.now(),
            judul = "ini judul",
            isi = "ini isi"
        )

        self.new_student = StudentProfile.objects.create(
            user = self.user,
            deskripsi = "Hello World",
            nama = "John Doe",
            foto_profil = "https://via.placeholder.com/300/",
            bidang_minat = "Literatur",
            saldo = 100000,
        )
    
    def testPublish(self):
        self.new_konten.isPublished = False
        self.new_konten.publish()

        self.assertEquals(self.new_konten.isPublished, True)

    def testStr(self):

        self.assertEquals(str(self.new_konten), self.new_konten.judul)
        self.assertEquals(str(self.new_profile), "Fasilkom")

    def testViewKonten(self):
        response = self.client.post(
            reverse("users:login"), data = {
                "username" : "rplkelompok12",
                "password" : "ayeaye321"
            }
        )
        self.new_konten.isPublished = False
        self.new_konten.publish()
        self.new_konten.save()
        found = resolve(reverse('konten:listKonten'))
        self.assertEqual(found.func, viewKonten)

        self.new_konten_publish = KontenKreatif.objects.create(
            penulis = self.new_profile,
            isPublished = True,
            tanggalPublish = datetime.datetime.now(),
            judul = "ini judul",
            isi = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 2910921003490348908431908439041431341341890341890413890431"
        )
        
        response = self.client.get(reverse('konten:listKonten'))
        self.assertEquals(response.status_code, 200)
        html = response.content.decode('utf8')
        self.assertIn('dipublish', html)
        self.assertTemplateUsed(response, 'viewKonten.html')
        self.assertContains(response, "Hapus")
        self.assertNotContains(response, "Publish Sekarang")
        self.assertContains(response, "ini judul")
        self.assertContains(response, "Lorem ipsum")
        self.assertContains(response, "2910921003490348908431908439041431341341890341890413890431")

    def testViewKontenNotLoggedIn(self):
        response = self.client.get(reverse("users:logout"))

        self.new_konten_publish = KontenKreatif.objects.create(
            penulis = self.new_profile,
            isPublished = True,
            tanggalPublish = datetime.datetime.now(),
            judul = "ini judul",
            isi = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 2910921003490348908431908439041431341341890341890413890431"
        )

        response = self.client.get(reverse("konten:listKonten"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Login untuk baca lebih lanjut")
        self.assertNotContains(response, "Hapus")
        self.assertNotContains(response, "Publish Sekarang")
        self.assertContains(response, "ini judul")
        self.assertContains(response, "Lorem ipsum")

    def testViewKontenLoggedInWithDifferentAccount(self):
        self.new_konten_publish = KontenKreatif.objects.create(
            penulis = self.new_profile,
            isPublished = True,
            tanggalPublish = datetime.datetime.now(),
            judul = "ini judul",
            isi = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 2910921003490348908431908439041431341341890341890413890431"
        )

        self.new_konten_publish = KontenKreatif.objects.create(
            penulis = self.new_profile,
            isPublished = False,
            tanggalPublish = datetime.datetime.now() + datetime.timedelta(days=5),
            judul = "ini belum dipublish",
            isi = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 2910921003490348908431908439041431341341890341890413890431"
        )
        
        self.new_user2 = User.objects.create_user(username="myname", password="bruteforcethis")
        self.new_user2.save()

        self.new_profile2 = StudentProfile.objects.create(
            user = self.new_user2,
            deskripsi = "Water in the fire",
            nama = "Inugami Korone",
            foto_profil = "https://static.wikia.nocookie.net/virtualyoutuber/images/0/05/Inugami_Korone_img.png/revision/latest/top-crop/width/360/height/450?cb=20190405185913",
            bidang_minat = "Gaming"
        )

        response = self.client.post(
            reverse("users:login"), data = {
                "username" : "myname",
                "password" : "bruteforcethis"
            }
        )

        response = self.client.get(reverse("konten:listKonten"))
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "Login untuk baca lebih lanjut")
        self.assertNotContains(response, "Hapus")
        self.assertNotContains(response, "Publish Sekarang")
        self.assertContains(response, "ini judul")
        self.assertContains(response, "Lorem ipsum")
        self.assertContains(response, "2910921003490348908431908439041431341341890341890413890431")
        self.assertNotContains(response, "ini belum dipublish")

    def testViewAutoPublish(self):
        response = self.client.post(
            reverse("users:login"), data = {
                "username" : "rplkelompok12",
                "password" : "ayeaye321"
            }
        )
        self.new_konten.isPublished = False
        self.new_konten.publish()
        self.new_konten.save()
        self.assertEquals(self.new_konten.isPublished, True)
        found = resolve(reverse('konten:listKonten'))
        self.assertEqual(found.func, viewKonten)

        self.new_konten_publish = KontenKreatif.objects.create(
            penulis = self.new_profile,
            isPublished = False,
            tanggalPublish = datetime.datetime.now() - datetime.timedelta(days=3),
            judul = "ini sudah lewat jdi harusnya sudah terpublish",
            isi = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 2910921003490348908431908439041431341341890341890413890431"
        )
        
        response = self.client.get(reverse('konten:listKonten'))
        self.assertEquals(response.status_code, 200)
        html = response.content.decode('utf8')
        self.assertIn('dipublish', html)
        self.assertTemplateUsed(response, 'viewKonten.html')
        self.assertContains(response, "Hapus")
        print(response.content)
        self.assertNotContains(response, "Publish Sekarang")
        self.assertContains(response, "ini sudah lewat jdi harusnya sudah terpublish")
        self.assertContains(response, "Lorem ipsum")
        self.assertContains(response, "2910921003490348908431908439041431341341890341890413890431")

    def testViewAddKonten(self):
        found = resolve(reverse('konten:addKonten'))
        self.assertEqual(found.func, addKonten)

        response = self.client.get(reverse('konten:addKonten'))
        self.assertEquals(response.status_code, 200)
        html = response.content.decode('utf8')
        self.assertIn('Konten', html)
        self.assertTemplateUsed(response, 'addKonten.html')
        
        response = self.client.post(
            reverse("users:login"), data = {
                "username" : "rplkelompok12",
                "password" : "ayeaye321"
            }
        )

        response = self.client.post(reverse('konten:addKonten'), data={
            'judul': 'ini judul 2', 
            'isi': 'ini isi', 
            'tanggalPublish': '2020-12-24'
        })

        self.assertEquals(response.status_code, 302)

        kontenKreatifs = KontenKreatif.objects.all()
        self.assertEquals(kontenKreatifs.count(), 2)

        konten1 = kontenKreatifs[0]
        konten2 = kontenKreatifs[1]
        self.assertEquals(konten1.judul, 'ini judul')
        self.assertEquals(konten2.judul, 'ini judul 2')

        self.assertEquals(konten1.id, 1)
        self.assertEquals(konten2.id, 2)

    def testViewDeleteKonten(self):
        self.client.login(username="rplkelompok12", password="ayeaye321")

        self.new_konten_delete = KontenKreatif.objects.create(
            penulis = self.new_profile,
            tanggalPublish = datetime.datetime.now(),
            judul = "ini judul",
            isi = "ini isi"
        )

        oldCount = KontenKreatif.objects.all().count()
        id_new_konten = self.new_konten_delete.id
        response = self.client.post(reverse("konten:deleteKonten", args=[id_new_konten]))
        deleteCount = KontenKreatif.objects.all().count()

        self.assertEquals(deleteCount, oldCount-1)

    def testViewPublishKonten(self):
        self.client.login(username="rplkelompok12", password="ayeaye321")

        self.new_konten_publish = KontenKreatif.objects.create(
            penulis = self.new_profile,
            tanggalPublish = datetime.datetime.now(),
            judul = "ini judul",
            isi = "ini isi"
        )

        id_new_konten = self.new_konten_publish.id
        response = self.client.post(reverse("konten:publishKonten", args=[id_new_konten]))
        bucketKonten = KontenKreatif.objects.get(id = id_new_konten)

        self.assertEquals(bucketKonten.isPublished, True)
