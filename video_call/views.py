from django.shortcuts import render, redirect
from courses.models import Appointment, Transaksi
from django.contrib import messages

# Create your views here.
def sesi_kursus_view(request, appointment_id):
    appointment = Appointment.objects.get(id=appointment_id)
    if not request.user.is_authenticated or appointment.status == "Selesai":
        return redirect("dashboard:upcoming")

    # Selesaikan appointment bila selesai.
    if request.method == "POST":
        appointment.status = "Selesai"
        appointment.save()
        
        messages.add_message(request, messages.SUCCESS, "Sesi kursus telah berhasil diselesaikan!")
        return redirect("dashboard:upcoming")


    # Get appointment ke berapa
    transaksi_apt = appointment.bagian_dari

    apt_lst = transaksi_apt.appointment_set.order_by("id")

    context = {
        "appointment" : appointment,
        "appointment_ke" : list(apt_lst).index(appointment) + 1,
        "total_apt" : len(transaksi_apt.appointment_set.all())
    }

    return render(request, "video_call/video_call.html", context=context)