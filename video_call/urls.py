from django.urls import path
from . import views

app_name = "video_call"

urlpatterns = [
    path("<int:appointment_id>", views.sesi_kursus_view, name="sesi_kursus")
]