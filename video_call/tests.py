from django.test import TestCase, LiveServerTestCase, Client
from django.urls import reverse, resolve

from django.contrib.auth.models import User

from users.models import StudentProfile
from courses.views import courses_list, subcourses_list, subcourses_detail, checkout

from courses.models import Course, Subcourse, Transaksi, Appointment
from datetime import datetime, timedelta
from uuid import uuid4

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_url = reverse("users:login")

        self.new_user = User.objects.create_user("user123", password="passworduser123")
        self.new_user.save()

        self.new_profile = StudentProfile.objects.create(
            user = self.new_user,
            deskripsi = "Hello World",
            nama = "John Doe",
            foto_profil = "https://via.placeholder.com/300/",
            bidang_minat = "Literatur"
        )

        # Login sebagai user123
        self.client.post(
            self.login_url, data = {
                "username" : "user123",
                "password" : "passworduser123"
            }
        )

        self.sample_course = Course.objects.create(
                nama="Fisika",
                deskripsi="Fisika dasar, listrik, kuantum, dll."
            )

        self.sample_subcourse = Subcourse.objects.create(
            nama="Fisika Listrik",
            deskripsi="Fisika listrik, hukum kirchoff, hukum ohm, dll",
            jam_dibutuhkan=6,
            course=self.sample_course,
            harga=500000
        )

        self.sample_transaksi = Transaksi.objects.create(
            metode_pembayaran="Saldo",
            jumlah_pembayaran=500000,
            student_pembeli=self.new_profile,
            subcourse_dibeli=self.sample_subcourse,
            status="Dibayar"
        )

        self.sample_appointment = Appointment.objects.create(
            status="Pending",
            waktu_kursus=datetime.now() + timedelta(days=1),
            kode_video_call=str(uuid4()),
            bagian_dari = self.sample_transaksi
        )

        self.session_url = reverse("video_call:sesi_kursus", args=[self.sample_appointment.id])

    def test_sesi_kursus_page(self):
        response = self.client.get(
            self.session_url
        )

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            "video_call/video_call.html"
        )

    def test_selesaikan_kursus(self):
        response = self.client.post(
            self.session_url
        )

        self.assertEquals(
            "Selesai",
            Appointment.objects.first().status
        )

    def test_akses_kursus_selesai_redirects(self):
        sample_appointment = self.sample_appointment
        sample_appointment.status = "Selesai"
        sample_appointment.save()

        response = self.client.get(
            self.session_url
        )

        self.assertEquals(response.status_code, 302)


