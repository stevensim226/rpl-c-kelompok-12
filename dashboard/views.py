from django.shortcuts import render, redirect
from django.contrib import messages
from courses.models import Appointment
from users.models import StudentProfile

day = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']

def upcoming(request):
    if not request.user.is_authenticated:
        return redirect("users:login")
    if request.method == "POST":
        messages.add_message(request, messages.SUCCESS, "Pembelian sesi kursus berhasil!")

    user = StudentProfile.objects.get(user=request.user.id)
    context = {"apts" : [], "user" : user}
    for apt in user.get_upcoming_apt():
        nama_subcourse = apt.bagian_dari.subcourse_dibeli.nama
        apt_list = Appointment.objects.filter(bagian_dari=apt.bagian_dari).order_by("waktu_kursus")
        for i in range(apt_list.count()):
            if apt_list[i] == apt:
                sesi = "Sesi kursus ke-" + str(i+1) + " dari " + str(apt_list.count())
                break
        tanggal = str(day[apt.waktu_kursus.weekday()]) + ", " + str(apt.waktu_kursus.day) + ' ' + str(month[apt.waktu_kursus.month-1]) + ' ' + str(apt.waktu_kursus.year)
        waktu = str(apt.waktu_kursus.hour) + ":" + str(apt.waktu_kursus.minute) + " - " + str((apt.waktu_kursus.hour + 1) % 24) + ":" + str(apt.waktu_kursus.minute)
        context["apts"].append([nama_subcourse, sesi, tanggal, waktu, apt.id])
    return render(request, "dashboard/upcoming.html", context)