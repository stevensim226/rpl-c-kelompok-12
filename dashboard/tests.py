from django.test import TestCase, Client
from django.urls import reverse, resolve

from django.contrib.auth.models import User

from .views import upcoming
from users.models import StudentProfile
from courses.models import Course, Subcourse, Transaksi, Appointment

from datetime import datetime, timedelta

class TestUrls(TestCase):
    def setUp(self):
        self.upcoming_url = reverse("dashboard:upcoming")

    def test_upcoming_url(self):
        upcoming_func = resolve(self.upcoming_url).func
        self.assertEquals(upcoming, upcoming_func)

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.date_now = datetime.now()
        self.new_user = User.objects.create_user("myname", password="bruteforcethis")
        self.new_user.save()

        self.new_profile = StudentProfile.objects.create(
            user = self.new_user,
            deskripsi = "Water in the fire",
            nama = "Inugami Korone",
            foto_profil = "https://static.wikia.nocookie.net/virtualyoutuber/images/0/05/Inugami_Korone_img.png/revision/latest/top-crop/width/360/height/450?cb=20190405185913",
            bidang_minat = "Gaming"
        )
        
        self.new_course = Course.objects.create(
            nama="Fisika",
            deskripsi="Mata pelajaran kesukaan kita semua"
        )

        self.new_subcourse = Subcourse.objects.create(
            nama="Fisika Mekanika",
            deskripsi="Ilmu yang mempelajari fungsi dan pelaksanaan mesin",
            jam_dibutuhkan=2,
            course=self.new_course,
            harga=250000
        )

        self.new_transaksi = Transaksi.objects.create(
            metode_pembayaran="Virtual Account",
            jumlah_pembayaran=500000,
            student_pembeli=self.new_profile,
            subcourse_dibeli=self.new_subcourse,
            status="Dibayar"
        )

        self.new_appointment = Appointment.objects.create(
            status="Accepted",
            waktu_kursus=self.date_now + timedelta(days=1),
            kode_video_call="rxe-xhfv-epy",
            bagian_dari = self.new_transaksi
        )

        self.new_appointment2 = Appointment.objects.create(
            status="Accepted",
            waktu_kursus=self.date_now + timedelta(days=2),
            kode_video_call="rxe-xhfv-epy",
            bagian_dari = self.new_transaksi
        )
        
    def test_not_authenticated(self):
        response = self.client.get("/dashboard/", follow=True)
        self.assertRedirects(response, "/login")
        self.assertTemplateUsed(response, "users/login.html")
        self.assertTemplateNotUsed(response, "dashboard/upcoming.html")

    def test_authenticated(self):
        self.client.login(username="myname", password="bruteforcethis")
        response = self.client.get("/dashboard/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "dashboard/upcoming.html")
        self.assertContains(response, self.new_profile.nama)
        self.assertContains(response, self.new_subcourse.nama)

    def test_past_appointment(self):
        self.new_appointment3 = Appointment.objects.create(
            status="Ended",
            waktu_kursus=self.date_now - timedelta(days=1),
            kode_video_call="rxe-xhfv-epy",
            bagian_dari = self.new_transaksi
        )
        self.client.login(username="myname", password="bruteforcethis")
        response = self.client.get("/dashboard/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "dashboard/upcoming.html")
        self.assertNotContains(response, "Sesi kursus ke-1 dari 3")
        self.assertContains(response, "Sesi kursus ke-3 dari 3")

    def test_pembelian_berhasil(self):
        self.client.login(username="myname", password="bruteforcethis")
        response = self.client.post("/dashboard/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "dashboard/upcoming.html")
        self.assertContains(response, "Pembelian sesi kursus berhasil!")

    def test_no_upcoming_appointment(self):
        self.new_user2 = User.objects.create_user("goodluck", password="UXylcxdX8yqi7_u16gJsEKJX3nLEXdm7kQECjygRYLhhtqA2qqxk0yEiZcC512rgK9ZmxgvKNUX9wOP_dCaG1UcLhFZ9oj6V8-fYskL4")
        self.new_user2.save()

        self.new_profile2 = StudentProfile.objects.create(
            user = self.new_user2,
            deskripsi = "My little pogchamp",
            nama = "Ryuko",
            foto_profil = "https://i.kym-cdn.com/photos/images/newsfeed/001/945/570/977.jpg",
            bidang_minat = "Idol"
        )

        self.client.login(username="goodluck", password="UXylcxdX8yqi7_u16gJsEKJX3nLEXdm7kQECjygRYLhhtqA2qqxk0yEiZcC512rgK9ZmxgvKNUX9wOP_dCaG1UcLhFZ9oj6V8-fYskL4")
        response = self.client.get("/dashboard/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "dashboard/upcoming.html")
        self.assertContains(response, self.new_profile2.nama)
        self.assertNotContains(response, self.new_subcourse.nama)
        self.assertContains(response, "Kamu tidak memiliki appointment, ayo daftar di subcourse yang tersedia")

    def test_different_subcourse(self):
        self.new_subcourse2 = Subcourse.objects.create(
            nama="Sejarah Fisika",
            deskripsi="Pelajari latar belakang penemuan-penemuan besar yang membentuk dunia yang kita tinggali saat ini. Mulai dari penemuan listrik hingga penemuan bom atom, ayo kita pelajari semuanya!",
            jam_dibutuhkan=1,
            course=self.new_course,
            harga=150000
        )

        self.new_transaksi2 = Transaksi.objects.create(
            metode_pembayaran="Virtual Account",
            jumlah_pembayaran=150000,
            student_pembeli=self.new_profile,
            subcourse_dibeli=self.new_subcourse2,
            status="Dibayar"
        )

        self.new_appointment3 = Appointment.objects.create(
            status="Accepted",
            waktu_kursus=self.date_now + timedelta(days=5),
            kode_video_call="rxe-xhfv-epy",
            bagian_dari = self.new_transaksi2
        )

        self.client.login(username="myname", password="bruteforcethis")
        response = self.client.get("/dashboard/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "dashboard/upcoming.html")
        self.assertNotContains(response, "Sesi kursus ke-3 dari 3")
        self.assertContains(response, "Sesi kursus ke-1 dari 2")
        self.assertContains(response, "Sesi kursus ke-1 dari 1")

    def test_date(self):
        day = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
        month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        self.new_appointment3 = Appointment.objects.create(
            status="Ended",
            waktu_kursus=self.date_now - timedelta(days=1),
            kode_video_call="rxe-xhfv-epy",
            bagian_dari = self.new_transaksi
        )

        appointment_date = f"{day[self.new_appointment.waktu_kursus.weekday()]}, {self.new_appointment.waktu_kursus.day} {month[self.new_appointment.waktu_kursus.month-1]} {self.new_appointment.waktu_kursus.year}"
        appointment_date2 = f"{day[self.new_appointment2.waktu_kursus.weekday()]}, {self.new_appointment2.waktu_kursus.day} {month[self.new_appointment2.waktu_kursus.month-1]} {self.new_appointment2.waktu_kursus.year}"
        appointment_date3 = f"{day[self.new_appointment3.waktu_kursus.weekday()]}, {self.new_appointment3.waktu_kursus.day} {month[self.new_appointment3.waktu_kursus.month-1]} {self.new_appointment3.waktu_kursus.year}"
        
        self.client.login(username="myname", password="bruteforcethis")
        response = self.client.get("/dashboard/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "dashboard/upcoming.html")
        self.assertContains(response, appointment_date)
        self.assertContains(response, appointment_date2)
        self.assertNotContains(response, appointment_date3)

    def test_time(self):
        appointment_time = f"{self.new_appointment.waktu_kursus.hour}:{self.new_appointment.waktu_kursus.minute} - {(self.new_appointment.waktu_kursus.hour + 1) % 24}:{self.new_appointment.waktu_kursus.minute}"
        self.client.login(username="myname", password="bruteforcethis")
        response = self.client.get("/dashboard/")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "dashboard/upcoming.html")
        self.assertContains(response, appointment_time)
