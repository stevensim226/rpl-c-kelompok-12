## Tugas Kelompok 5 RPL-C Kelompok 12
Link website : https://kursus-plus.herokuapp.com/

Link website development : https://kursus-plus-dev.herokuapp.com/

Development Coverage: [![Dev Coverage](https://gitlab.com/stevensim226/rpl-c-kelompok-12//badges/development/coverage.svg?style=flat-square)](https://gitlab.com/stevensim226/rpl-c-kelompok-12/-/tree/development)

Nama / NPM:
1. Jeremy Victor Andre Napitupulu - 1906293114
2. Steven - 1906293322
3. Steven Wiryadinata Halim - 1906350622
4. Valentino Herdyan Permadi - 1906350660

## Instalasi
Buat virtual environment Python di local machine anda, lalu clone repository ini, dan jalankan
```
pip3 install -r requirements.txt
```